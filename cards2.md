


# ATLAS 25 - The history of the ATLAS Experiment... and some personal notes

## Card 1 - Letter of Intent

year:  1992
title: The birth of the ATLAS Experiment 
image: 1_1992_ATLAS_LOI.JPG
link:  https://cdsweb.cern.ch/record/291061/

text:

Experiments are born when a group of scientists proposes their construction. The ATLAS Experiment was born in 1992, on the 1st of October, when a team of physicists from 88 research institutes and universities around the world co-signed and published the so-called "Letter of Intent." In the public document, they proposed the design and the construction of <i>"a General-Purpose pp Experiment at the Large Hadron Collider at CERN"</i>.


note:

In 1992...well, I was still in high-school! And, I must admit, I did not consider to study physics, yet. At that time, instead, I had the idea of studying naval engineering after the school, and I bought a lot of yachting and sailing magazines!



## Card 2 - The den of the giant is ready!

year:  2003
title: The den of the giant is ready!
image: 2_2003_cavern.jpg
link:  http://cds.cern.ch/record/620604

text: 

The excavation work under the Swiss country is completed. The underground cavern which will host the ATLAS detector is ready! At that time, if I remember correctly, it was the largest artificial cavern in the world. 

In the picture, Peter Jenni, the ATLAS spokesman at that time (the first on the left), the CERN Director General Luciano Maiani, and other personalities from the Swiss Confederation government and CERN, are visiting the cavern just before the inauguration.  


note: 

I remember that day very well! I joined the ATLAS Experiment precisely at that time. In those months I was at CERN for the first time, as a Summer Student, and I was working on my Bachelor diploma work, within the ATLAS Data Acquisition group. 

On the inauguration day, on the 4th of June 2003, I visited the new cavern with many other ATLAS people. The cavern was huge. And really empty! In fact, its emptiness made it look even larger! I still remember the feeling of being there, together with other ATLAS people: even as a mere young student, I felt as I was part of a scientific collaboration, already.

Small note: at that time, nobody could even image that exactly 9 years later, on the same day, the ATLAS Collaboration will announce one of the most fundamental discoveries of the last decades: the discovery of the Higgs boson!



    
image link: https://mediastream.cern.ch/MediaArchive/Photo/Public/2003/0306003/0306003_02/0306003_02-A4-at-144-dpi.jpg
 

## Card 3 - Lowering the first magnet

year:  2004
title: Lowering the first magnet
image: 3_2004_building_toroid.jpg
link:  https://cds.cern.ch/record/802466; https://cds.cern.ch/record/892334

link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2004/0411003/0411003_05/0411003_05-A4-at-144-dpi.jpg

text: 

The construction catwalks are now built, the cavern is ready to host the first pieces of the ATLAS detector. It is the 26th of October  2004. The first superconducting magnet is installed: attached through a cable to a crane on the surface, its 100 tonnes are lowered for 100 meters vertically through the pit into the cavern.

note: 

At that time I was in Rome, studying for my Master degree in Particle Physics. I had got a Bachelor degree (the Italian "Laurea", to be precise) the year before, with a diploma work on parallel programming techniques for the software of the  <i>data flow</i> of the <a href="https://atlas.cern/discover/detector/trigger-daq">ATLAS data acquisition system</a>; but I did not know yet that I would have pursued my physics career on the ATLAS experiment.





## Card 4 - A huge detector!

year:  2005
title: A huge detector!
image: 4_2005_toroid.jpg
link:  https://cds.cern.ch/record/910381

link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2005/0511013/0511013_01/0511013_01-A4-at-144-dpi.jpg

text: 

In this iconic image, one can realize the size of the ATLAS detector, compared to the person in the bottom part of the picture. The detector is not completely built yet, but the eight toroid magnets are now in place; we can also see the calorimeter at the center, before being moved to the center of the detector. 

But why do we need such big magnets? Charged particles passing through a magnetic field are bent, proportionally to the particle's energy and the strength of the magnetic field: the higher the particle's energy, the stronger the magnetic field has to be to bend the particle. Thus, if we know the strength of the magnetic field and we measure how much the particle bends along its trajectory, we can infer its energy. That is why in ATLAS we need such enormous magnets: being the energy of the particles produced in the LHC collisions very high, we need a very strong magnetic field to bend them enough to be able to measure their trajectories accurately.  

The eight big magnets in the picture are part of the outer system of the ATLAS detector, the so-called <a href="https://atlas.cern/discover/detector/muon-spectrometer"><i>muon spectrometer</i></a>,  designed to reveal and measure the properties of the particles called <a href="https://home.cern/about/physics/standard-model"><i>muons</i></a>.


note: 

November 2005... if I remember well, at that time I was studying Theoretical Physics in Rome, towards my Master degree (the Italian "Laurea Specialistica," indeed). I didn't know that I would have done a Ph.D. on the ATLAS experiment, yet!






## Card 5 - From all over the world, to work together

year:  2008
title: We come from all over the world to work together towards a common goal 
image: 5_2008_ATLAS_physicists.jpg
link:  http://cds.cern.ch/record/1110951

link to image: http://cds.cern.ch/record/1110951

text:

The ATLAS experiment has been designed, and it is run, by a collaboration of physicists. At the time of writing this little note, we are about 3000 physicists, coming from 182 universities and institutes from 38 different countries. We can take a look at the list of ATLAS authors in <a href="https://twiki.cern.ch/twiki/bin/view/AtlasPublic/Publications">all papers published by the ATLAS Collaboration</a>.

We can also take a look at the list of <a href="http://atlas.web.cern.ch/Atlas/Management/Institutions.html">all institutes which are part of the ATLAS Collaboration</a>, as well as <a href="https://atlas.cern/discover/collaboration">visualizing them on an interactive map</a>. 

Let us browse through the countries listed there, and we will realize how ATLAS is an example of a successful project, gathering people coming from such different countries to work together in a harmonious way towards a common goal. Even people coming from countries that are officially in conflict, here in ATLAS they work together. 

Science, education, and knowledge are and should be above borders, conflicts,  political problems or mere national issues. Science should always gather ideas and people from all places and all social situations. Moreover, science should always be open and accessible to everyone: all brilliant minds should be able to contribute to the common knowledge, without barriers and without being excluded because of the history, the nationality, the gender, their personality, their handicaps and the personal attitudes and preferences.

The picture shows several hundreds of ATLAS physicists coming from US institutes (out of about 2000 at the time it was taken, in 2008). It is a really nice picture, I think,: it can give a sense of the size of the ATLAS collaboration; even if the number of today's ATLAS physicists are much larger than those represented in the picture.        


personal note: 

At the time of that picture I was back at CERN. Since two years already, I had started working on the ATLAS experiment again, and since March 2008 I was based at CERN, sent there by the University of Freiburg, where I was studying for my Ph.D. in Particle Physics.

As said, the picture shows a group of physicists coming from US institutes. At that time, I didn't know that, a few years later, I would have been employed by a US university, as well!


 
other stuff:
For later... Running an experiment as a collaboration means that there is ???




## Card 6 - The last pieces

year:  2008
title: The last pieces
image: 6_2008_MuonSmallWheels.jpg
link:  https://cds.cern.ch/record/1082464

text: 

The two last big pieces of the ATLAS detector are ready at building 182 on CERN site, waiting to be lowered into the underground cavern. They are the two ATLAS "muon small wheels," which have been designed and built to detect the particles called "muons" created in the collision, which travel through the forward and backward regions of the detector. Those two big pieces would be placed one at each end of the ATLAS detectors, like two caps.



note:

In those years I was working on the development of data analysis strategies to reveal hints of the existence of  <a href="https://home.cern/about/physics/supersymmetry"><i>Supersymmetric</i> particles</a>. We did not have experimental data yet  ---the data taking period would start later that year---, but we created simulated data to test our analysis tools. 

Also, after the start of data taking,  I started taking shifts in the <a href="https://atlas.cern/tags/control-room">ATLAS control room</a>, at the "Muon desk." There, I monitored the good and correct behavior of muon chambers; among them, the "small wheels" shown in the picture. 

For a Ph.D. student like me, it was very cool being there in the Control Room: it was like being in the pilothouse of a big, sophisticated ship!






## Card 7 - 4 muons for a possible Higgs boson

year:  2011
title: 4 muons for a Higgs boson
image: 7_2011_event_display_4muons.jpg
link: https://cds.cern.ch/record/1406057


text: 

In pictures like this one, called "event displays," we can take a look at what happens inside the ATLAS detector. 

When the two proton beams from LHC collide, many particles are created because of the high energy of the collision; some of those particles interact with each other, some others decay giving birth to other particles, others aggregate. When a particle interacts with the detectors, signals are created, revealing us where that particle has passed, its energy and its nature. 

In the figure here we can see what happened in the inner part of the detector, just after that specific collision. Two heavy <i>Z</i> particles are created, which, after a very short life, decay giving birth to four particles called "muons": the 4 red tracks coming out from the center. This type of event is often called the "golden one" because it is an event which is free of large "noise" and it is considered one of the best types of event where the Higgs boson could be found. 

The image is from 2011. At that time it was not considered produced by the decay of a Higgs boson yet because the measurements had to be verified with more data. 

Many other types of events are studied by the ATLAS experiment, and <a href="https://twiki.cern.ch/twiki/bin/view/AtlasPublic/EventDisplayPublicResults">visualized in event displays</a>.




note: 

At that time I was part of the <a href="https://atlas.cern/discover/detector/trigger-daq">ATLAS Data Acquisition group</a>; I was in the second year of my CERN Fellowship, and I was working on the Run Control system. The Run Control is the system which takes care of the correct configuration of all the subsystems which compose the ATLAS detector. Each piece has to be switched on and off at the right time, and correctly configured, to assure a successful data taking.

I remember the discussions about the first events which could be a sign of the presence of a Higgs boson. Of course, the events had to be confirmed by taking a number of events which could be considered statistically significant; but the excitement was already increasing...


  

## Card 8 - The BIG announcement!

year:  2012
title: The BIG announcement!
image: 8_2012_HiggsEnglert.jpg
link:  https://cds.cern.ch/record/1459503

text:

...and, finally, the announcement! The two general-purpose experiments at the LHC ---ATLAS and CMS--- have finally collected and analyzed a fair amount of events showing features compatible with the Higgs boson and, on the 4th of June 2012, they jointly announce its discovery! 

In the picture, Peter Higgs and Francois Englert are waiting for the start of the seminar which will confirm the theory they started 50 years earlier. 



note:

On that day...well, I was at work at CERN, of course! Everybody was waiting for the joint seminar. Of course, we from ATLAS already know the content of the ATLAS talk; and rumors had run, in the days before, about the content of the talk from the other experiment, CMS. The excitement was high. Personally, I was looking forward to seeing Higgs' and Englert's reaction. After the announcement, it has been nice to see the joy and the satisfaction on their faces.

Small note: on the 4th of June 2003 the ATLAS cavern was inaugurated. Nine years later, one of the biggest discovery in particle physics was announced!


  
    


## Card 9 - A toast for the Higgs boson!

year:  2012
title: A toast for the Higgs boson!
image: 9_2012_higgs_celebration.jpg
link:  https://cds.cern.ch/record/1475204

text:

On August 2012, after the public announcement of the Higgs boson discovery on June the 4th, the ATLAS collaboration celebrated the big discovery. We all gathered in one of the big halls at CERN (in particular, the one which had earlier hosted the  LHC magnets). We were all there: the physicists working on the detector, those handling the experimental data, the ones working on data analysis, those developing the software to run the experiment. All there, because all contributed to the discovery, and all co-signed the scientific paper claiming that such important discovery.


note:

Of course, I was there, celebrating our big success! You can find me in the picture if you know how I looked like at that time... 
[if not, you can find me here! ;-) ] ??? link


## Card 10 - Selecting, cross-checking, and verifying

year:  2014
title: Selecting, cross-checking, and verifying
image: 10_2014_event_display_H2e2mu.png
link:  http://cds.cern.ch/record/1756226

text:

The picture shows what happened a candidate event of a Higgs boson decaying into two electrons (shown as blue lines) and two muons (red lines). The azure cylinder cutout at the center shows a part of the ATLAS calorimeter, while the tubes are part of the structure supporting the muon system. The blue squares represent some of the so-called "muon chambers," detectors used to reveal and measure the particles called "muons." The chambers shown in the picture are those traversed by the muons created by the decay of the Higgs boson.


note:

I remember when we worked to produce this specific event display. Publishing an event display is not a task to be taken lightly: you release a visualization of what really happened inside the detector at the moment of that specific collision. And you want to be sure that everything is correct. 

In those days, we needed to produce some Higgs event displays. For days the team working on this task filtered experimental data, reconstructed events, cross-checked the selection of physics objects (electrons, protons, muons, jets, and so forth) and searched for the best way to visualize our data.

Moreover, by visualizing experimental data in event displays, we can interactively cross-check and verify the correctness of the statistical analysis we perform on the large datasets collected by the experiment. By picking, visualizing, and inspecting single events, for instance, we can check if we selected the right collision events, and if we reconstructed the physics objects from detector measurements in the right way. By visualizing experimental data, we can also verify if the simulation was correctly written and run, if the geometry of the detector has been rightly described, and, in the end, if our data analysis in its entirety has been correctly designed and handled. 



official caption:
 
Display of a Higgs --> 2e2μ candidate revealed in the ATLAS detector. EventNumber : 76170653 RunNumber : 209109. The Higgs boson candidate, produced at the interaction point, decays into two electrons (blue lines) and two muons (red lines). The two leading particle jets (yellow cones) have pT = 180 and 150 GeV. More details here: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2013-21/





## Card 11 - A pinch of lead ions, for the primordial taste!

year:  2015
title: A pinch of lead ions, for the primordial taste!
image: 11_2015_event_display_HI.png
link:  https://cds.cern.ch/record/2115422/

text:

The picture shows a very crowded collision between two bunches of heavy lead ions. For the most of the data taking period, LHC collides bunches of protons. As a result, a large quantity of energy is collected at the impact spot. For some specific research lines, however, the quantity of energy collected in the tiny space of the collision point is not enough: a larger, so-called, <i>energy density</i> is required, that is a larger quantity of energy in the same tiny space. 

One of those is the study of the so-called <a href="https://home.cern/about/physics/heavy-ions-and-quark-gluon-plasma"><i>quark-gluon plasma</i></a>. This studies the primordial status of Universe just a few instants after the great Big Bang: a <i>plasma</i> ---let us think of it as a dense fluid--- of free <a href="https://home.cern/about/physics/standard-model">quarks and gluons</a>, which are the components of all the ordinary matter we know today. Under normal conditions, it is not possible to separate them apart and make them free. However, if we get a very large quantity of energy in a very narrow space ---that is, a high <i>energy density</i>---, then we can replicate the conditions we think they were present just after the Big Bang, and we can study the behavior of the quarks and gluons in much more detail.

One way to obtain a large energy density at LHC is changing the colliding particles. By using "heavier" particles, we can get higher energy density at the collision point. Lead nuclei contain many protons and neutrons, making them ideal candidates to act as heavier colliding objects. The lead atoms are deprived of their electrons, transforming them into "lead ions," and then injected into the LHC and accelerated. The energy produced by the collisions of those bunches of <i>heavy-ions</i> is high enough to keep the quarks and the gluons free for an instant, before colling down and recombining together in a plethora of particles. Those are visualized in the picture like yellow tracks.

This picture shows one of the first lead ions collisions of 2015, and it is part of the ATLAS events shown at the LHC seminar on physics results from the data taking period known as "Run2" (the talk was given by M. Kado, on behalf of the whole ATLAS Collaboration). 


note:

I remember very well when we were preparing the event displays for the public seminar. Among other types of event, to present both our latest technical achievements and physics results, we wanted to show a heavy-ion collision as well. 
The team working on this spent some days searching for the right event, reconstructing it and trying different visualization, in order to show the complexity in the analysis of heavy-ion collisions and to illustrate our findings.

I personally really like this image: I find it very detailed, as well as very illustrative for the general public, ...and with the right amount of the "Wow" factor!


 
official caption:

One of the first heavy-ion collisions with stable beams recorded by ATLAS in November 2015. Tracks reconstructed from hits in the inner tracking detector are shown as orange arcs curving in the solenoidal magnetic field. The green and yellow bars indicate energy deposits in the Liquid Argon and Scintillating Tile calorimeters respectively. The beam pipe and the inner detectors are also shown.

info:

- ATLAS events shown at the LHC seminar on physics results from run2
- Event displays included in the talk : "Results with the Full 2015 Data Sample from the ATLAS Experiment", Marumi Kado (LAL, Orsay and CERN) on behalf of the ATLAS Collaboration. , 15.12.2015





## Card 12 - Upgrading the experiment, towards new challenges

year:  2017
title: Upgrading the experiment, towards new challenges
image: 12_2017_upgrade.jpg
link:  https://cds.cern.ch/record/1702006

text:

The activities around and on the ATLAS detector never stop. To run at the energy frontier and to get the most from the collisions provided by the LHC, new techniques and methods are studied, and the detector has to be updated from time to time. 

In the picture, taken in 2014, ATLAS members are installing a new layer of particle detectors close to the interaction point, where the particle beams, accelerated by the LHC, collide. 

Beside the "normal" update (if we can use the word "normal" when talking about a detector which has the size of a cathedral and about the weight of the Eiffel tower!), studies for future modifications and upgrades are ongoing as well.
  
In these years, LHC is happily running and we, the experiments, are taking data out of that. But there are plans to extend the accelerator's possibilities in the near future. When that happens, the number of collisions will increase, and the experiments on the accelerator have to be prepared to take that increased amount of data. 

Hence, while a group of ATLAS physicists dedicates his daily work to the data taking and the analysis of those data, another team is working on the upgrade of the experiment to match our future requirements.    

When we talk about the "upgrade," we do not consider the detector alone. The software used to run the experiment has to be upgraded as well, to be able to effectively handle the amount of data we will have in the future and to correctly and efficiently exploit the possibilities offered by the modern computing hardware.

Since several years, we are in the process of upgrading our software: we are modernizing it by using the latest coding standards, and we are updating our software framework to make use of the most recent techniques and advances in software development. 
 


note:

Since several years, I am involved in the design, development, and maintenance of the so-called "Core Software," the set of tools which compose the software framework of the ATLAS experiment. In particular, I am involved in the development of software tools to visualize data interactively, with 3D graphics, and to handle the data storing the geometrical description of the detector.

