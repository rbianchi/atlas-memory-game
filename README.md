Created by Riccardo Maria BIANCHI <riccardo.maria.bianchi@cern.ch>

Code stored at: https://gitlab.cern.ch/rbianchi/atlas-memory-game/

Fork of an original work by Nate Wiley (http://codepen.io/natewiley/pen/HBrbL) on Codepen (http://codepen.io/riclab/pen/rzyVWO).

---

A JavaScript memory game on the *first* 25 years of the ATLAS Experiment!

Click the cards and find matches to win.. you know how to play it

