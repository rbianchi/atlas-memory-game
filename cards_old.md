


----

# Old cards

## Card 1
title: US LHC scientists in building 40
image: collab/0806010_01-A5-at-72-dpi.jpg
link to image: http://cds.cern.ch/record/1110951
link on the card: http://cds.cern.ch/record/1110951


## card 2
year: 2012
title: Higgs and Englert
Higgs and Englert in the Main Auditorium at CERN, just before the beginning of the ATLAS-CMS joint seminar, where the experiments announced the discovery of the Higgs boson - 
asset: collab/1207136_08-A5-at-72-dpi.jpg
link to image: https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-HI-1207136%2008,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2012/1207136/1207136_08/1207136_08-A5-at-72-dpi.jpg
link on the card: https://cds.cern.ch/record/1459503

text:

note:


## card 3
year: 2015
title: ATLAS logo
asset: collab/ATLAS-Logo-Blue-invert-RVB-S.png
link to image: http://atlas-outreach.web.cern.ch/sites/atlas-outreach.web.cern.ch/files/ATLAS-Logo-Blue-invert-RGB-H_0.jpg
links on the card: http://atlas-outreach.web.cern.ch/design/downloads, https://indico.cern.ch/event/365815/contributions/867595/

## card 4

year: 2012
title: ATLAS Higgs discovery celebration
asset: collab/higgs_celebration.jpg
link to image: https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-GE-1208180%2001,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2012/1208180/1208180_01/1208180_01-A5-at-72-dpi.jpg
link on the card: https://cds.cern.ch/record/1475204

## card 5
title: Building the ATLAS detector
asset: constr/0511013_01-A5-at-72-dpi.jpg
link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2005/0511013/0511013_01/0511013_01-A4-at-144-dpi.jpg
link on the card: https://cds.cern.ch/record/910381

## card 6
year: 2008
title: The two ATLAS Muon Wheels 
asset: constr/0801014_02-A5-at-72-dpi.jpg
link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2008/0801014/0801014_02/0801014_02-A4-at-144-dpi.jpg
link on the card: https://cds.cern.ch/record/1082464

The two ATLAS small wheels, last big pieces to descend the ATLAS cavern, are ready at building 181 on CERN site.


## card 7
title: ATLAS upgrade
asset: constr/A99V746b2.jpg
link to image: https://cds.cern.ch/record/1702006/files/A99V746b2.jpg?subformat=icon-1440
link on the card: https://cds.cern.ch/record/1702006

## card 8
title: The ATLAS cavern
asset: constr/inst-2004-186.jpg
link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2004/0411003/0411003_05/0411003_05-A4-at-144-dpi.jpg
link on the card: https://cds.cern.ch/record/802466; https://cds.cern.ch/record/892334



## card 9
title: ATLAS Heavy Ions event
asset: displays/ATLAS_VP1_event_display_HI_run286665_evt419161_2015-11-25T11-12-50_v4.png
link to image: https://cds.cern.ch/record/2115422/files/ATLAS_VP1_event_display_HI_run286665_evt419161_2015-11-25T11-12-50_v4.png?subformat=icon-1440
link on the card: https://cds.cern.ch/record/2115422/


## card 10
year: 2011
title: ATLAS Higgs in 4 muons event
asset: displays/1112301_01-A5-at-72-dpi.jpg
link to image: https://cds.cern.ch/record/1406057
link on the card: https://cds.cern.ch/record/1406057

In pictures like this one, called "event displays", one can take a look at what happens inside our ATLAS detector. When the two proton beams from LHC collide, many particles are created because of the high energy of the collision; some of those particles interact with other ones, some decay giving birth to other particles, others aggregate. When a particle interact with the detectors, a signal is created, revealing us where that particle has passed, its energy and its nature. In the figure here we can see what happened in the inner part of the detector, just after the collision. Two heavy Z particles are created, which, after a very short life, decay giving birth to four particles called "muons": the 4 red tracks coming out from the center. This type of event is known as the "golden one", because it is an event which is free of large "experimental noise" and it is considered one of the best events where the Higgs boson could be found. 

The image is from 2011. At that time it was not considered produced by the decay of an Higgs boson yet, because the measurements had to be verified with a larger amount of data. 


## card 11

year: 2014
title: ATLAS Higgs in 2 muons and 2 electrons event
asset: displays/event_display_H2e2mu_run209109_evt76170653_2012-08-24T09-31-00.png
link to image: http://cds.cern.ch/record/1756226
link on the card: http://cds.cern.ch/record/1756226


## card 12

year: 2015
title: ATLAS multijet event
asset: displays/event_display_vp1_run265545_evt2501742_2015-05-21T09-58-30.png
link to image: http://cds.cern.ch/record/2017847
link on the card: http://cds.cern.ch/record/2017847


