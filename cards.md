
# Cards by topics 

## event displays:

https://cds.cern.ch/record/2017847
https://cds.cern.ch/record/1406057
https://cds.cern.ch/record/1756226
http://cds.cern.ch/record/2105605


## celebrations and collaboration:

https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-GE-1208180%2001,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2012/1208180/1208180_01/1208180_01-A5-at-72-dpi.jpg
https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-HI-1207136%2008,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2012/1207136/1207136_08/1207136_08-A5-at-72-dpi.jpg
https://cds.cern.ch/record/1110951

## ATLAS Logo

link???

## construction and upgrade:

https://cds.cern.ch/record/1702006/files/A99V746b2.jpg?subformat=icon-640
https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-EX-0801014%2002,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2008/0801014/0801014_02/0801014_02-A5-at-72-dpi.jpg
https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-EX-0511013%2001,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2005/0511013/0511013_01/0511013_01-A5-at-72-dpi.jpg
https://cds.cern.ch/record/892334/files/inst-2004-186.jpg?subformat=icon-640


# Cards list:


## Card 1
title: US LHC scientists in building 40
image: collab/0806010_01-A5-at-72-dpi.jpg
link to image: http://cds.cern.ch/record/1110951
link on the card: http://cds.cern.ch/record/1110951


## card 2
title: Higgs and Englert
Higgs and Englert in the Main Aufitorium at CERN, just before the beginning of the ATLAS-CMS joint seminar, where the experiments announced the discovery of the Higgs boson - 
asset: collab/1207136_08-A5-at-72-dpi.jpg
link to image: https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-HI-1207136%2008,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2012/1207136/1207136_08/1207136_08-A5-at-72-dpi.jpg
link on the card: https://cds.cern.ch/record/1459503

## card 3
title: ATLAS logo
asset: collab/ATLAS-Logo-Blue-invert-RVB-S.png
link to image: http://atlas-outreach.web.cern.ch/sites/atlas-outreach.web.cern.ch/files/ATLAS-Logo-Blue-invert-RGB-H_0.jpg
link on the card: http://atlas-outreach.web.cern.ch/design/downloads 

## card 4
title: ATLAS Higgs discovery celebration
asset: collab/higgs_celebration.jpg
link to image: https://cds.cern.ch/stats/customevent_register?event_id=media_download&arg=CERN-GE-1208180%2001,photo,Medium,WEBSTAT_IP&url=https%3A//mediastream.cern.ch/MediaArchive/Photo/Public/2012/1208180/1208180_01/1208180_01-A5-at-72-dpi.jpg
link on the card: https://cds.cern.ch/record/1475204

## card 5
title: Building the ATLAS detector
asset: constr/0511013_01-A5-at-72-dpi.jpg
link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2005/0511013/0511013_01/0511013_01-A4-at-144-dpi.jpg
link on the card: https://cds.cern.ch/record/910381

## card 6
title: The two ATLAS Muon Wheels 
asset: constr/0801014_02-A5-at-72-dpi.jpg
link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2008/0801014/0801014_02/0801014_02-A4-at-144-dpi.jpg
link on the card: https://cds.cern.ch/record/1082464

## card 7
title: ATLAS upgrade
asset: constr/A99V746b2.jpg
link to image: https://cds.cern.ch/record/1702006/files/A99V746b2.jpg?subformat=icon-1440
link on the card: https://cds.cern.ch/record/1702006

## card 8
title: The ATLAS cavern
asset: constr/inst-2004-186.jpg
link to image: https://mediastream.cern.ch/MediaArchive/Photo/Public/2004/0411003/0411003_05/0411003_05-A4-at-144-dpi.jpg
link on the card: https://cds.cern.ch/record/802466; https://cds.cern.ch/record/892334



## card 9
title: ATLAS Heavy Ions event
asset: displays/ATLAS_VP1_event_display_HI_run286665_evt419161_2015-11-25T11-12-50_v4.png
link to image: https://cds.cern.ch/record/2115422/files/ATLAS_VP1_event_display_HI_run286665_evt419161_2015-11-25T11-12-50_v4.png?subformat=icon-1440
link on the card: https://cds.cern.ch/record/2115422/


## card 10
title: ATLAS Higgs in 4 muons event
asset: displays/1112301_01-A5-at-72-dpi.jpg
link to image: https://cds.cern.ch/record/1406057
link on the card: https://cds.cern.ch/record/1406057


## card 11
title: ATLAS Higgs in 2 muons and 2 electrons event
asset: displays/event_display_H2e2mu_run209109_evt76170653_2012-08-24T09-31-00.png
link to image: http://cds.cern.ch/record/1756226
link on the card: http://cds.cern.ch/record/1756226


## card 12
title: ATLAS multijet event
asset: displays/event_display_vp1_run265545_evt2501742_2015-05-21T09-58-30.png
link to image: http://cds.cern.ch/record/2017847
link on the card: http://cds.cern.ch/record/2017847


